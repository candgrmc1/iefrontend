import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { Helmet } from "react-helmet";

const DefaultFooter = React.lazy(() => import("./DefaultFooter"));
const DefaultHeader = React.lazy(() => import("./DefaultHeader"));
const Plans = React.lazy(() => import("../views/Plans"));
const Login = React.lazy(() => import("../views/Login"));
const HomePage = React.lazy(() => import("../views/HomePage"));

class DefaultLayout extends Component {
    loading = () => (
        <div className="animated fadeIn pt-1 text-center">Loading...</div>
    );

    render() {
        return (<div>

            <div className="content-container">
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>Interesting Engineering</title>
                </Helmet>
                <DefaultHeader />
                <Switch>
                    <Route
                        path="/login"
                        name="Login"
                        render={(props) => <Login {...props} />}
                    />
                    <Route
                        path="/plans"
                        name="Plans"
                        render={(props) => <Plans {...props} />}
                    />
                    <Route
                        path="/"
                        name="Home Page"
                        render={(props) => <HomePage {...props} />}
                    />
                </Switch>
                <div className="push"></div>
            </div>
            <DefaultFooter />
        </div>);
    }
}

export default DefaultLayout;