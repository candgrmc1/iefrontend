import React, { Component } from "react";
import { Link } from "react-router-dom";

class DefaultFooter extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }


    render() {
        return (
            <div className="default-footer">
                <div className="row m-auto container">
                    <div className="col-12 d-flex d-lg-none footer-logo-container">
                        <Link to="/"></Link>
                    </div>
                    <div className="col-12 d-flex d-lg-none footer-social-media">
                        <span className="m-auto">
                            <Link to="#" className="facebook"></Link>
                            <Link to="#" className="instagram"></Link>
                            <Link to="#" className="youtube"></Link>
                            <Link to="#" className="twitter"></Link>
                            <Link to="#" className="linkedin"></Link>
                            <Link to="#" className="flipboard"></Link>
                        </span>
                    </div>
                    <div className="col-12 d-flex d-lg-none footer-link-container">
                        <div className="row justify-content-center m-auto">
                            <Link to="#" className="col-auto">Privacy Policy</Link>
                            <Link to="#" className="col-auto"><span></span>Do Not Sell My Personal Information</Link>
                            <Link to="#" className="col-auto"><span></span>Advertising Guidelines</Link>
                            <Link to="#" className="col-auto"><span></span>Terms Conditions</Link>
                            <Link to="#" className="col-auto"><span></span>Responsible Disclosure</Link>
                            <Link to="#" className="col-auto"><span></span>Site Map</Link>
                            <div className="col-12 copyright-text-container text-center">
                                © Copyright 2021 | Interesting Engineering, Inc. | All Rights Reserved
                        </div>
                        </div>
                    </div>
                    <div className="d-none d-lg-flex col-lg-9 footer-link-container">
                        <div className="row">
                            <Link to="#" className="col-auto">Privacy Policy</Link>
                            <Link to="#" className="col-auto"><span></span>Do Not Sell My Personal Information</Link>
                            <Link to="#" className="col-auto"><span></span>Advertising Guidelines</Link>
                            <Link to="#" className="col-auto"><span></span>Terms Conditions</Link>
                            <Link to="#" className="col-auto"><span></span>Responsible Disclosure</Link>
                            <Link to="#" className="col-auto"><span></span>Site Map</Link>
                            <div className="col-12 copyright-text-container">
                                © Copyright 2021 | Interesting Engineering, Inc. | All Rights Reserved
                        </div>
                        </div>
                    </div>
                    <div className="d-none d-lg-flex col-lg-3 footer-logo-container">
                        <Link to="/"></Link>
                    </div>
                </div>
            </div>
        );
    }
}

export default DefaultFooter;