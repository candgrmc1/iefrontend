import React, { Component } from "react";
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from "react-router-dom";
import Logo from "../assets/interesting-engineering-logo.svg";
class DefaultHeader extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    logout = () => {

    };

    render() {
        // 
        const isLogin = true;
        return (
            <div className="default-header">
                <div className="row container m-auto">
                <div className="col navbar-container">
                    <Navbar expand={false} className="custom-navbar">
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Brand className="mr-auto" href="/">
                            <img src={Logo}></img>
                        </Navbar.Brand>

                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link href="/plans">SUBSCRIPTION</Nav.Link>
                                <Nav.Link href="#">NEED HELP?</Nav.Link>
                                {(() => {
                                    if (isLogin) {
                                        return (
                                            <Nav.Link href="#" onClick={this.logout}>Logout</Nav.Link>
                                        )
                                    }
                                })()}
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </div>

                {(() => {
                    if (!isLogin) {
                        return (
                            <div className="col right-link-container">
                                <Link className="search-link float-right" to="#"></Link>
                                <Link className="sign-in-link  float-right d-none d-lg-block" to="/login">SIGN IN</Link>
                                <Link className="subscribe-link  float-right d-none d-lg-block" to="#">SUBSCRIBE</Link>
                                <Link className="podcast-link  float-right d-none d-lg-block" to="#">PODCAST</Link>
                                <Link className="newsletter-link  float-right d-none d-lg-block" to="#">NEWSLETTER</Link>
                            </div>
                        )
                    } else {
                        return (
                            <div className="col right-link-container">
                                <Link className="search-link float-right" to="#"></Link>
                                <Link className="sign-in-link  float-right d-none d-lg-block" to="#">NEED HELP?</Link>
                            </div>
                        )
                    }
                })()}
</div>
            </div>
        );
    }
}

export default DefaultHeader;