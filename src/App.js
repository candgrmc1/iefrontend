import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import './App.scss';

const loading = () => (
  <div className="animated fadeIn pt-3 text-center">Loading...</div>
);

const DefaultLayout = React.lazy(() => import("./layouts/DefaultLayout"));

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading()}>
          <Switch>
            <Route
              exact
              path="/login"
              name="Login Page"
              render={(props) => <DefaultLayout {...props} />}
            />
            <Route
              exact
              path="/"
              name="Home Page"
              render={(props) => <DefaultLayout {...props} />}
            />
            <Route
              path="/"
              name="Private"
              render={(props) => <DefaultLayout {...props} />}
            />
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
