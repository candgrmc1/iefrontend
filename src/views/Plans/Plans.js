import React, { useState, useEffect } from "react";
import "../../styles/plans.scss"
import BannerImage from "../../assets/plans-banner-image.png";
import { Link } from "react-router-dom";
import {post,get} from '../../request'
import { useHistory } from 'react-router';



const Plans = () => {
    const [showPlanDetail,setShowPlanDetail] = useState([])
    const [plans, setPlans] = useState([])
    const [loading, setLoading] = useState(false)
    const history = useHistory()

   useEffect(()=> {
       const token = localStorage.getItem('authToken')
       if(!token){
           history.replace('/login')
       }
       setLoading(true)
       get({
           url: '/plans'
       }).then( r => setPlans(r),setLoading(false))
   },[])
    const showHideDetail = (id) => {
        const index = showPlanDetail.indexOf(id)
        let arr = showPlanDetail
        if(index > -1){
            arr.splice(index,1) 
        }else{
            arr.push(id)
        }

        setShowPlanDetail(arr)
    }

        return (
            <div className="plans-page">
                <div className="page-header">
                    <div className="container row m-auto">
                        <div className="col-12 col-lg-6 plan-main-info">
                            <h1>IE+ PLANS FOR <span>SUBSCRIPTION</span></h1>
                            <h2>You can <b>cancel anytime.</b></h2>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper.
                            </p>
                            <img className="notebook-img" src={BannerImage}/>
                        </div>
                        <div className="col-12 col-lg-6">

                            <div className="row m-auto plan-link-container">
                                <div className="col">
                                    <Link to="#" className="plan-link active">Individual</Link>
                                </div>
                                <div className="col">
                                    <Link to="#" className="plan-link">Enterprise</Link>
                                </div>
                                <div className="col">
                                    <Link to="#" className="plan-link">Student</Link>
                                </div>
                            </div>

                            {
                                !loading ? plans.map(i => {
                                    return (
                                        <>
                                    <div className="col-12 plan-detay">
                                        <span className="plan-title">{i.title}</span>
                                        <p className="plan-description">Full access to all features and usnlimited product updates. Speacial discounts.</p>
                                        {showPlanDetail.indexOf(i.id) > -1 && (
                                            <div className="plan-detail">
                                                <p>Unlimited access to IE+</p>
                                                <p>Full Access to InterestingEngineering.com,  InterestingEngineering Shop,  InterestingEngineering mobile and tablet apps</p>
                                                <p>IE videos and podcasts</p>
                                                <p>The Open and Close, subscriber-only newsletters with everything you need to know, to start and end each day</p>
                                            </div>
                                        )}
                                        <div className="row mt-4">
                                            <div className="col" style={{ maxWidth: "50px" }}><span className="plan-price">$5</span></div>
                                            <div className="col"><span className="plan-no-discount">$8</span></div>
                                            <div className="coltext-right"><Link to="/payment" className="payment-button">SUBSCRIBE</Link></div>
                                        </div>
                                    </div>
                                    <Link to="#" onClick={()=>showHideDetail(i.id)} className="show-hide-detail">{showPlanDetail.indexOf(i.id) > -1 ? 'hide details' : 'show details' }</Link>
                                    
                                    </>
                                    )
                                }) : ''
                            }
                            


                                 
                                 

                            <div className="col-12 other-plan-detay">
                                <div className="row">
                                    <div className="col-10 plan-title">
                                        Quarterly
                                    </div>
                                    <div className="col-2 plan-price">
                                        $18
                                    </div>
                                    <div className="col-10 plan-desc">
                                        Full access to all features and unlimited product updates. Speacial discounts.
                                    </div>
                                    <div className="col-2 plan-no-discount">
                                        $25
                                    </div>
                                </div>
                            </div>

                            <div className="col-12 other-plan-detay">
                                <div className="row">
                                    <div className="col-10 plan-title">
                                        Annual
                                    </div>
                                    <div className="col-2 plan-price">
                                        $35
                                    </div>
                                    <div className="col-10 plan-desc">
                                        Full access to all features and unlimited product updates. Speacial discounts.
                                    </div>
                                    <div className="col-2 plan-no-discount">
                                        $42
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div className="page-content">

                    <p>Page Content...</p>
                </div>

            </div>
        );
}

export default Plans;