import React, { Component } from "react";
import "../../styles/home-page.scss";
import BannerImage from "../../assets/banners/home-page-banner.jpg";
import ArticleImage from "../../assets/article-images/radio.jpg";
import { Link } from "react-router-dom";
class HomePage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }


    render() {
        return (
            <div className="home-page">
                <div className="banner">
                    <span>ADVERTISEMENT</span>
                    <img src={BannerImage} />
                </div>
                <div className="article-container container">
                    <h2>This Speaker and Projector In One Offers Vivid Images and Crisp Sound</h2>
                    <p>The device projects up to 60" screen while the speaker produces 12W of sound.</p>
                    <div className="author-container">
                        <span className="author-name">Trevor English</span>
                        <span className="date">December 16, 2020</span>
                    </div>
                    <div className="article-type-share row">
                        <div className="article-type col text-left">INNOVATION</div>
                        <div className="article-share text-right">
                            <Link to="#" className="facebook"></Link>
                            <Link to="#" className="twitter"></Link>
                            <Link to="#" className="mail"></Link>
                            <Link to="#" className="more"></Link>
                        </div>
                    </div>
                </div>
                <img className="article-image" src={ArticleImage}></img>
                <div className="row m-auto article-image-info">
                    <div className="col-12 col-md-8">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor.<br />
                        Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper.
                    </div>
                    <div className="col-12 col-md-4 text-right">
                        Martin Scorsese | AFP News Agency
                    </div>
                </div>
                <div className="container article-content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rutrum nec justo ac tempor. Suspendisse consequat purus lacus, egestas varius mi pharetra at. Phasellus ullamcorper dui ut fringilla semper. Sed pellentesque iaculis nisl, a venenatis lectus tempor sit amet. Sed volutpat felis ut orci semper, at tempus felis euismod. Donec commodo magna eget sapien cursus, id tristique eros rhoncus. Duis fringilla metus nulla, semper elementum urna vestibulum eu. Mauris in venenatis lacus, eget gravida nulla. Quisque vel lorem efficitur urna facilisis dapibus fermentum eget enim. Nullam aliquam dictum molestie. Fusce odio nunc, aliquet id tempus vel, fringilla nec dui. Praesent at justo gravida, rhoncus tortor vitae, malesuada tellus.
                </div>

                <div className="bottom-subscribe-container">
                    <p>WE ARE PUBLISHING EXCLUSIVE ARTICLES EVERY WEEK. ENJOY YOUR 3 FREE ARTICLES.</p>
                    <Link to="/"></Link>
                </div>
            </div>
        );
    }
}

export default HomePage;