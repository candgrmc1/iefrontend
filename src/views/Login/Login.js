import React, { Component, useState,useEffect } from "react";
import { useHistory } from 'react-router';

import "../../styles/login.scss";
import { Form, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import FacebookLoginButton from "../../assets/facebook-login-button.svg";
import GoogleLoginButton from "../../assets/google-login-button.svg";
import LinkedinLoginButton from "../../assets/linkedin-login-button.svg";
import {post,get} from '../../request'

const Login = () => {
   const [email,setEmail] = useState('')
   const [password,setPassword] = useState('')
   const history = useHistory()


   const handleLogin = async () => {
       console.log({email,password,post})
       const {access_token:authToken} = await post({
           url: '/login',
           body: {
               email,
               password
           },
           onFail: (m) =>{
            console.log({m})
           }
       })
       localStorage.setItem('authToken',authToken)
        const me =  await post({
        url: '/user/me',
      
        onFail: (m) =>{
         console.log({m})
        }
        })

        localStorage.setItem('user','me')
        history.replace('/')
        return false
   }

    return (
        <div className="login-page container">
            <div className="row m-auto">
                <div className="col login-title p-0">
                    LOGIN
                </div>
                <div className="col login-title-seperator p-0"></div>
            </div>
            <form
                method="post"
                className="form-horizontal"
            >
                <Form.Group>
                    <Form.Control onChange={ e => setEmail(e.target.value)} required className="email-address" size="lg" type="text" name="email" placeholder="Email address" />
                </Form.Group>
                <Form.Group>
                    <Form.Control onChange={ e => setPassword(e.target.value)}  className="password" size="lg" type="password" name="password" placeholder="Password" />
                </Form.Group>
                <Button className="login-button" variant="info" block onClick={handleLogin}>LOGIN</Button>{' '}
            </form>
            <Button className="login-email-button" variant="info" block>EMAIL A LOGIN LINK</Button>{' '}

            <div className="row m-auto or-container">
                <div className="col or-seperator"></div>
                <div className="col or-text">OR</div>
                <div className="col or-seperator"></div>
            </div>

            <div className="row m-auto other-login-container">
                <div className="col pl-0">
                    <Link to="#"><img src={FacebookLoginButton} className="w-100"></img></Link>
                </div>
                <div className="col">
                    <Link to="#"><img src={GoogleLoginButton} className="w-100 google-login-button"></img></Link>
                </div>
                <div className="col pr-0">
                    <Link to="#"><img src={LinkedinLoginButton} className="w-100"></img></Link>
                </div>
            </div>
            <Link to="#" className="no-account">Don’t have an account? <span>Subscribe</span></Link>
        </div>
    );
}

export default Login;