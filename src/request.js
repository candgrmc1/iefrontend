import axios from 'axios';
import {API_URL} from './Constants'


export const post = async ({headers = {},body,url, onFail}) => {
    const completeUrl = API_URL + url
    let response
    try{
        // intercept
        const token = localStorage.getItem('authToken')
        if(token && token !== "undefined"){
            headers['Authorization'] = `Bearer ${token}`
            response = await axios.post(completeUrl,body,{
                headers
            })
            if(response.status !== 200){
                
                throw new Error(`status code => ${response.status} || ${response.statusText}`)
                
            }
        }else{
            response = await axios.post(completeUrl, body)
            console.log({res:response})
            if(response.status !== 200){
                // ok
                
                throw new Error(`status code => ${response.status} || ${response.statusText}`)
            }

        }
        const data = response.data

        return data


    }catch(e){
       
        if(onFail !== undefined){
            onFail(e.response?.data?.message)
            return false
        }
    }
}

export const get = async ({headers={},url,onFail}) => {
    const completeUrl = API_URL + url
    const token = localStorage.getItem('authToken')
    let response
    try{
        if(token && token !== "undefined"){
            headers['Authorization'] = `Bearer ${token}`

            response = await axios.get(completeUrl,{
                headers
            })

            if(response.status === 200){
                return response.data
            }
        } 
        else{
            response = await axios.get(completeUrl)

            if(response.status === 200){
                return response.data
            }
        }
    }catch(e){
        const error = e.response?.data?.error
        const message = e.response?.data?.message
       
        if(onFail !== undefined){
            onFail(message || "Internal Error")
            return false
        }
    }

    

    
}

